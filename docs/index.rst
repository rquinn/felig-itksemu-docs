.. itks-hccstar-hwemu documentation master file, created by
   sphinx-quickstart on Fri May 28 14:53:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ITk Strips HCCStar Hardware Emulator
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   hdl/packetgen/HCCStarEmu.rst
   hdl/lcbdecoder/itks_lcb_wrapper.rst
