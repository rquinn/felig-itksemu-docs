Packer Data Selector
============================

The packer data selector connects the packer to either the register read
packet generator FIFO or the hit generator FIFO, depending on which has data.
The hit generator FIFO always has priority. Once a read is started, however,
the packer will read from the same FIFO until it has finished reading the
current packet.


Signals
-------

Inputs
  ``clk``
    240 MHz clock
  ``rst``
    Reset signal
  ``hitgen_data_i[21:0]``
    ``dout`` from the HitGen FIFO
  ``hitgen_empty_i``
    ``empty`` from the HitGen FIFO
  ``hitgen_valid_i``
    ``valid`` from the HitGen FIFO
  ``rrpgen_data_i[21:0]``
    ``dout`` from the Register Read Packet FIFO
  ``rrpgen_empty_i``
    ``empty`` from the Register Read Packet FIFO
  ``rrpgen_valid_i``
    ``valid`` from the Register Read Packet FIFO
  ``packer_ready_i``
    Input from the packer signaling it's ready for new data

Outputs
  ``hitgen_rd_en_o``
    ``rd_en`` signal for the HitGen FIFO
  ``rrpgen_rd_en_o``
    ``rd_en`` signal for the Register Read Packet FIFO
  ``packer_data_o[15:0]``
    Words to be sent to the packer
  ``packer_length_o[4:0]``
    Length of words to be sent to the packer
  ``packer_last_o``
    Signals the current word is the last one in the packet
  ``packer_valid_o``
    Signals valid data for the packer

Internal
  ``fifo_data[21:0]``
    General 22-bit bus to contain the data from whichever FIFO is selected.
  ``fifo_valid``
    Signal indicating when ``fifo_data`` is valid
  ``state``
      Of type ``t_states``, stores the state of the SM


Description
-----------

The packer data selector acts as a sort of mux, forwarding the packer's commands and input lines to either the hit packet fifo or the register read packet fifo, depending on which has data. The 22-bit FIFO words are split into their component 16-bit data word, 5-bit length, and 1-bit "last word" flag. The hit packet FIFO will always be read before the register read packet FIFO. The state machine is shown below.

.. tikz:: State machine diagram for the packer data selector.
   :libs: automata, positioning, arrows

   [->,
    >=stealth, node distance=3cm,
    every state/.style={
       rectangle,
       rounded corners,
       draw=black,
       very thick,
       minimum height=2em,
       inner sep=2pt,
       text centered,
       fill=gray!10
    },
    initial text=$ $
   ]
   \node[state, initial] (s0) {Idle};
   \node[state, above right of=s0, xshift=2cm] (s1) {HitPacket};
   \node[state, below right of=s0, xshift=2cm] (s2) {RegReadPacket};
   \draw   (s0) edge[loop above, above left] node{Empty FIFOs} (s0)
            (s0) edge[below right] node{Hit FIFO Data} (s1)
            (s1) edge[loop above] node{Not Last Word} (s1)
            (s1) edge[above left, bend right] node{Last Word} (s0)
            (s0) edge[above right] node{Reg Read FIFO Data} (s2)
            (s2) edge[loop below] node{Not Last Word} (s2)
            (s2) edge[below left, bend left] node{Last Word} (s0);

The ``fifo_data`` and ``fifo_valid`` intermediates are hooked up to whichever FIFO has been selected. ``fifo_data`` is then split up into the three output components for the packer.

A clock cycle could be saved here (when there's multiple packets in the FIFOs) if the state machine doesn't always return to the Idle state and instead could stay in either of the reading states.
