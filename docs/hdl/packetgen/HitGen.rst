Hit Generator
==============================================

The hit generator generates and stores physics packets when trigger commands
are received. The generator's control module takes trigger and hit information
in from the event builder and the BRAM and generates a variable-length physics
packet. This packet is written into a FIFO in the format expected by the
packer. The FIFO is hooked into the packer data selector, so the stored packets
will eventually be read by the packer and sent out on the uplink.

Signals
-------

Inputs
  ``clk``
    240 MHz clock input.
  ``rst``
    Synchronous reset.
  ``trig_preamble[10:0]``
    Trigger information from the event builder.
  ``empty_trig``
    Signals when the event builder FIFO is empty.
  ``valid_trig``
    Signals when ``trig_preamble`` is valid.
  ``chunk_length[11:0]``
    *Unused*.
  ``itks_emu_hits[16:0]``
    ABCStar clusters from the BRAM.
  ``itks_emu_hits_valid``
    Signals when ``itks_emu_hits`` is valid.
  ``itks_emu_hits_empty``
    Signals when the BRAM is empty.
  ``itks_emu_hits_full``
    *Unused*. Signals when the BRAM is full.
  ``itks_emu_hits_almost_full``
    *Unused*. Signals when the BRAM is almost full.
  ``itks_emu_rd_ctl``
    *Unused*. BRAM control bit passed from the PCIe interface.
  ``rd_en_hitgen``
    Read enable for the HitGen FIFO, from the packer data selector.

Outputs
  ``data[21:0]``
    Output from the HitGen FIFO, passed to the packer data selector.
  ``rd_en_trig``
    Read enable for the event builder FIFO.
  ``itks_emu_rd_en``
    Read enable for the BRAM.
  ``empty_hitgen_out``
    Signals when the HitGen FIFO is empty, passed to the packer data selector.
  ``valid_hitgen_out``
    Signals when ``data`` is valid, passed to the packer data selector.

Internal
  ``data_ctl_out[21:0]``
    Data output from the control module, gets passed to ``din`` of the FIFO.
  ``data_sig[21:0]``
    Data output from the FIFO, becomes the ``data`` output signal.
  ``wr_en_hitgen``
    Write enable from the control module for the FIFO.
  ``empty_hitgen``
    Signals when the FIFO is empty, becomes ``empty_hitgen_out``.
  ``valid_hitgen``
    Signals when ``data_sig`` is valid, becomes ``valid_hitgen_out``.
  ``full_hitgen``
    *Unused*. Signals when the FIFO is full.
  ``almost_full_hitgen``
    *Unused*. Signals when the FIFO is almost full.

Components
----------
.. toctree::
   :maxdepth: 2

   HitGen_Ctl.rst

