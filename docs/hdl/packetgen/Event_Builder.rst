Event Builder
=============


The event builder creates 11-bit trigger information blocks using the L0a tag
and BCID and stores them in a FIFO.

**Note:** even though the clocks here (and in the event counter) are described
as 40 MHz, they are actually 240 MHz. Probably a good idea to modify all this.

Signals
-------

Inputs
  ``clk_40_i``
    40 MHz clock input.
  ``rst``
    Reset.
  ``bcr_i``
    Bunch crossing counter reset.
  ``trig_sel_i``
    Switches between internal and external L0a tag.
  ``start_event``
    Signals when to start an event (for internal L0a counting).
  ``l0a_i``
    L0a trigger.
  ``l0a_tag_i[6:0]``
    L0a tag.
  ``rd_en_event``
    Read enable for the internal FIFO.

Outputs
  ``event_info_o[10:0]``
    Output from the FIFO, contains the 11-bit trigger information. Of type
    ``event_info``.
  ``empty_event``
    Signals when the internal FIFO is empty.
  ``valid_event``
    Signals when ``event_info_o`` is valid.

Internal
  ``event_info_s[10:0]``
    Connects the output of the event counter to ``din`` of the FIFO.
  ``wr_en``
    Write enable for the internal FIFO -- comes from the event counter.
  ``full``
    *Unused.* Signals when the internal FIFO is full.
  ``almost_full``
    *Unused.* Signals when the internal FIFO is almost full.


Components
----------
.. toctree::
   :maxdepth: 2

   Event_Counter.rst
